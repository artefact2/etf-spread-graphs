<?php

if(!isset($_GET['t']) || !isset($_GET['d']) || !isset($_GET['s'])) die(0);

$f = fopen(__DIR__.'/data.csv', 'a');
flock($f, LOCK_EX);
fprintf($f, "%s,%s,%s\n", $_GET['t'], $_GET['d'], $_GET['s']);
fflush($f);
flock($f, LOCK_UN);
fclose($f);
