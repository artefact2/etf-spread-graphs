SIZE=32cm,18cm
DATA=data.csv
DEFAULTS=out/freq-all.pdf out/points-all.pdf out/lines-MWRD,XDWD,IWDA,LCWD,SWRD.pdf out/lines-EESM,ZPRX,ZPRV.pdf

default-pdf: $(DEFAULTS)

default-png: $(patsubst %.pdf, %.png, $(DEFAULTS))

host:
	php -S '[::1]:24497' -t .

out:
	mkdir $@

clean:
	rm -Rf out

.PHONY: clean

out/ticker-all.csv: $(DATA) out
	cp --reflink=auto $< $@

out/ticker-%.csv: $(DATA) out
	grep -P '^('`echo $@ | cut -d- -f2 | cut -d. -f1 | tr ',' '|'`'),' $< > $@.tmp || (rm -f $@.tmp; exit 1)
	mv $@.tmp $@

out/points-%.pdf: out/ticker-%.csv
	gnuplot -e 'set terminal pdf color enhanced size '$(SIZE) -c points.gnuplot $< >> $@.tmp || (rm -f $@.tmp; exit 1)
	mv $@.tmp $@

out/lines-%.pdf: out/ticker-%.csv
	gnuplot -e 'set terminal pdf color enhanced size '$(SIZE) -c lines.gnuplot $< >> $@.tmp || (rm -f $@.tmp; exit 1)
	mv $@.tmp $@

out/freq-%.pdf: out/ticker-%.csv
	gnuplot -e 'set terminal pdf color enhanced size '$(SIZE) -c freq.gnuplot $< >> $@.tmp || (rm -f $@.tmp; exit 1)
	mv $@.tmp $@

out/%.png: out/%.pdf
	inkscape -d 300 -b white -e $@ $<
