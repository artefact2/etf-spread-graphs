set datafile separator ","

set ylabel "Cumulative frequency"
set xlabel "Spread (basis points)"

set key outside center top horizontal width 5
set grid xtics ytics
set xrange [0:*<50]
set yrange [0:1] noextend
set xtics 5
set ytics .1

plot for [ticker in system(sprintf("cut -d, -f1 %s | sort | uniq", ARG1))] sprintf("< grep %s %s | ./clean.php", ticker, ARG1) using 3:(1.) title ticker smooth cnormal with linespoints pn 10 lw 2
