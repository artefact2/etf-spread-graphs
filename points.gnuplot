set datafile separator ","

set ylabel "Spread (basis points)"

unset key
set grid xtics ytics
set xrange [*:*]
set yrange [0:*<60]

tickers = system(sprintf("./clean.php < %s | cut -d, -f1 | sort | uniq", ARG1))
unset xtics
do for [i=1:words(tickers)] {
   set xtics add (word(tickers, i) i)
}

set style fill transparent solid 0.1 noborder
set style circle radius 0.02

plot for [i=1:words(tickers)] sprintf("< grep %s %s | ./clean.php", word(tickers, i), ARG1) every 2 using (i+.1*invnorm(rand(0))):(column(3)+.5*invnorm(rand(0))) with circles lc 1
