#!/usr/bin/env php
<?php

const CONTINUOUS_START = 8*3600 + 5*60;  /* 9.05am local time */
const CONTINUOUS_STOP = 16*3600 + 30*60; /* 5.30pm local time */

while(($l = fgets(STDIN)) !== false) {
	$l = explode(',', substr($l, 0, -1));

	$ts = intval($l[1]) / 1000 % (86400);
	if($ts < CONTINUOUS_START || $ts > CONTINUOUS_STOP) continue;

	echo implode(',', $l), PHP_EOL;
}
