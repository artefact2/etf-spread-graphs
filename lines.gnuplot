set datafile separator ","

set xlabel "Time of day (UTC)"
set ylabel "Spread (3m D1/D5/D9, basis points)"

set key outside center top horizontal width 5
set grid xtics ytics
set xrange [*:*] noextend
set yrange [0:*<100]

set format x "%s"
set xtics format "%H:%M" time

set style fill transparent solid 0.1 noborder

tickers = system(sprintf("./clean.php < %s | cut -d, -f1 | sort | uniq", ARG1))

plot for [i=1:words(tickers)] sprintf("< grep %s %s | ./clean.php | ./bins.php 180000", word(tickers, i), ARG1) \
	 using (column(2)/1000):8 title word(tickers, i) with linespoints pn 10 lc i, \
	 for [i=1:words(tickers)] sprintf("< grep %s %s | ./clean.php | ./bins.php 180000", word(tickers, i), ARG1) \
	 using (column(2)/1000):6:10 with filledcurves lc i notitle
