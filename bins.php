#!/usr/bin/env php
<?php

$binwidth = intval($argv[1]);
$data = [];

while(($line = fgets(STDIN)) !== false) {
	$line = explode(',', substr($line, 0, -1));
	for($i = 2; $i < count($line); ++$i) {
		$data[$line[0]][floor(intval($line[1]) / $binwidth)][$i - 2][] = $line[$i];
	}
}

ksort($data);
$i = 0;
$ntickers = count($data);
foreach($data as $ticker => $sdata) {
	foreach($sdata as $bin => $ssdata) {
		echo $ticker, ",", ($ntickers === 1 ? ($bin + .5) : ($bin + .25 + .5 * ($i / ($ntickers - 1)))) * $binwidth;

		foreach($ssdata as $values) {
			sort($values);
			$nvalues = count($values);

			echo ",", $average = (array_sum($values) / $nvalues);
			echo ",", sqrt(array_sum(array_map(function($v) use($average) { return pow($v - $average, 2); }, $values)) / $nvalues); /* XXX: divide by N-1, handle N=1 case */
			echo ",", $values[0];
			echo ",", $values[floor($nvalues * .1)];
			echo ",", $values[floor($nvalues * .25)];
			echo ",", $values[floor($nvalues * .5)];
			echo ",", $values[floor($nvalues * .75)];
			echo ",", $values[floor($nvalues * .9)];
			echo ",", $values[$nvalues-1];
		}

		echo PHP_EOL;
	}

	++$i;
}
